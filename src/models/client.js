import { DataTypes } from 'sequelize';

export default (sequelize) => {
    const Client = sequelize.define('client', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lastName:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        secondLastName:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        
    }, {});

    return Client;
};