import { DataTypes } from 'sequelize';

export default (sequelize) => {
    const Sale = sequelize.define('sale', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        client: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references:{
                model:'clients',
                id: 'id'
            }
        },
        import: {
            type: DataTypes.FLOAT,
        },
        status:{
            type:DataTypes.INTEGER
        }
    }, {});

    return Sale;
};