import { DataTypes } from 'sequelize';

export default (sequelize) => {
    const ProductSale = sequelize.define('productsale', {
        idProduct: {
            type: DataTypes.INTEGER,
            references: {
                model:'products',
                key:'id'
            },
        },
        idSale: {
            type: DataTypes.INTEGER,
            references: {
                model:'sales',
                key:'id'
            },
        },
    }, {});

    return ProductSale;
};