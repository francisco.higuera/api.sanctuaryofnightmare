import user from './user';
import product from './product';
import client from './client';
import sale from './sale';
import productSale from './productSale';


export default (sequelize) => {
  const userModel = user(sequelize);
  const productModel = product(sequelize);
  const clientModel = client(sequelize);
  const saleModel = sale(sequelize);
  const productSaleModel = productSale(sequelize);
  return {
    userModel,
    productModel,
    clientModel,
    saleModel,
    productSaleModel
  };
}
