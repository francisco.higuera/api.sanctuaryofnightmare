const Sequelize = require('sequelize');

export default (callback) => {
	const sequelize = new Sequelize('sanctuary', 'francisco_higuera', 'panchito', {
		host: '50.21.179.154',
		dialect: 'postgres',
		operatorsAliases: false,
		port: 5432,
		pool: {
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000,
		},
	});

	sequelize
  .authenticate()
  .then(() => {
		callback(sequelize);
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });
};
