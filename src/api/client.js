import express from 'express';
import { returnError, returnData, getOffset } from './utils';

export default (sequelize, Client) => {
  const router = express.Router();

  router
  .route('/getClientByID')
  .get(async ({body}, res) => {
      try {
          const { id } = body;
          const client = await Client.findByPk(id);
          if(client != undefined)
          returnData(res, client);
          else
          returnData(res, "El Cliente no existe");
      } catch (e) {
          returnError(res, 500, e);
      }
  });

  router
  .route('/addClient')
  .post(async ({ body }, res) => {
      try {
          const { name, lastName, secondLastName } = await body;
          const client = await Client.build({
              name: name,
              lastName: lastName,
              secondLastName: secondLastName,
          }).save();
          returnData(res, client);
      } catch (e) {
          returnError(res, 500, e);
      }
  });

  router
  .route('/updateClient')
  .put(async ({ body }, res) => {
      try {
          const { id, name, lastName, secondLastName } = await body;
          const client = await Client.findByPk(id);
          if(client != undefined){
            client.name = name,
            client.lastName = lastName,
            client.secondLastName = secondLastName
          };
         client.save();
          returnData(res, client);
      } catch (e) {
          returnError(res, 500, e);
      }
  });

  router
  .route('/deleteClientByID')
  .get(async ({body}, res) => {
      try {
          const { id } = body;
          const client = await Client.findByPk(id);
          if(client != undefined){
              client.destroy();
              returnData(res, "Cliente eliminado con exito");
            }
          else
          returnData(res, "El Cliente no existe");
      } catch (e) {
          returnError(res, 500, e);
      }
  });
  return router;
}