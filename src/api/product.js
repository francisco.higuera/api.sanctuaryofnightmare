
import express from 'express';
import { returnError, returnData, getOffset } from './utils';
export default (sequelize, Product) => {
    const router = express.Router();

    router
        .route('/getProductByID')
        .get(async ({body}, res) => {
            try {
                const { id } = body;
                const product = await Product.findByPk(id);
                if(product != undefined)
                returnData(res, product);
                else
                returnData(res, "El Producto no existe");
            } catch (e) {
                returnError(res, 500, e);
            }
        });

    router
        .route('/addProduct')
        .post(async ({ body }, res) => {
            try {
                const { name, price, amount, url, description } = await body;
                const product = await Product.build({
                    name: name,
                    price: price,
                    amount: amount,
                    url: url,
                    description: description
                }).save();
                returnData(res, product);
            } catch (e) {
                returnError(res, 500, e);
            }
        })

        router
        .route('/updateProduct')
        .put(async ({body}, res) => {
            try {
                const {id, name, price, amount, url, description} = body;
                const product = await Product.findByPk(id);
                if(product != undefined){
                product.name = name;
                product.price = price;
                product.amount = amount;
                product.url = url;
                product.description = description;
                await product.save();
                returnData(res, product);
                }else{
                    returnData(res, "El producto no existe");
                }
                
            } catch (e) {
                returnError(res, 500, e);
            }
        });

        router
        .route('/deleteProductByID')
        .get(async ({body}, res) => {
            try {
                const { id } = body;
                const product = await Product.findByPk(id);
                if(product != undefined){
                    product.destroy();
                returnData(res, "Producto borrado con exito");
                }
                else
                returnData(res, "El Producto no existe");
            } catch (e) {
                returnError(res, 500, e);
            }
        });
    return router
}