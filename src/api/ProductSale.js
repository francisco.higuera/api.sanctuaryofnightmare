import express from 'express';
import { returnError, returnData, getOffset } from './utils';
export default (sequelize, ProductSale) => {
    const router = express.Router();

    router
    .route('/addProductSale')
    .post(async ({ body }, res) => {
        try {
            const { idSale, idProduct} = await body;
            const product = await ProductSale.build({
                idProduct: idProduct,
                idSale: idSale,
            }).save();
            returnData(res, product);
        } catch (e) {
            returnError(res, 500, e);
        }
    })

    router
    .route('/removeFromProductSaleByIdProduct')
    .post(async ({ body }, res) => {
        try {
            const {idProduct} = await body;
            const product = await ProductSale.findAll({
                where:{
                    idProduct: idProduct,
                }
            });
            if(product != undefined){
                product.destroy();
                returnData(res, "Producto eliminado con exito");
            }else 
                returnData(res, "El carrito no contiene el producto");
        } catch (e) {
            returnError(res, 500, e);
        }
    })

    return router;
}