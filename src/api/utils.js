import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

export const returnError = (res, status, message) => {
  console.error(message);
  res.status(status).json({ error: true, message });
};

export const returnData = (res, data) => {
  res.status(200).json({ error: false, data });
};

export const getOffset = (req) => {
  try {
    const offset = parseInt(req.query.skip);
    return offset || 0;
  } catch (e) {
    return 0;
  }
}

export const createAdminUser = async (User) => {
  const hashedPassword = await bcrypt.hash('12345678', 10);
  const role = 'admin';
  const newUser = await User
		.build({
			password: hashedPassword,
			email: 'pmedina.c95@gmail.com',
			username: 'pedromedina95',
			name: 'Pedro',
			lastname: 'Medina',
			secondLastName: 'Contreras',
			role,
		})
		.save();
	// TODO: CHANGE SECRET FOR: process.env.APP_SECRET
	await jwt.sign({ userId: newUser.id, role }, 'secret');
}

export const createClientUser = async (User)  => {
  const hashedPassword = await bcrypt.hash('12345678', 10);
  const role = 'client';
  const newUser = await User
		.build({
			password: hashedPassword,
			email: 'pedro.cliente@gmail.com',
			username: 'pedroclient',
			name: 'Pedro Cliente',
			lastname: 'Medina',
			secondLastName: 'Contreras',
			role,
		})
		.save();
	// TODO: CHANGE SECRET FOR: process.env.APP_SECRET
	await jwt.sign({ userId: newUser.id, role }, 'secret');
}

export const createTechnicalUser = async (User)  => {
  const hashedPassword = await bcrypt.hash('12345678', 10);
  const role = 'technical';
  const newUser = await User
		.build({
			password: hashedPassword,
			email: 'pedro.soporte@gmail.com',
			username: 'pedrosoporte',
			name: 'Pedro Soporte',
			lastname: 'Medina',
			secondLastName: 'Contreras',
			role,
		})
		.save();
	// TODO: CHANGE SECRET FOR: process.env.APP_SECRET
	await jwt.sign({ userId: newUser.id, role }, 'secret');
}
