import express from 'express';
import Sequelize, { Utils } from 'sequelize';
import bcrypt from 'bcryptjs';
import { returnError, returnData, getOffset } from './utils';

export default (sequelize, User) => {
  const router = express.Router();

  router
    .route('/')
    .get(async (req, res) => {
      try {
        // const offset = getOffset(req);
        //const filter = { limit: 10, offset };
        const hashedPassword = await bcrypt.hash('12345678', 10);
        const role = 'admin';
        const newUser = await User
          .build({
            password: hashedPassword,
            email: 'panshito02@gmail.com',
            username: 'panshito02',
            name: 'Francisco',
            lastname: 'Higuera',
            secondLastName: 'Zamarripa',
            role,
          })
          .save();
        returnData(res, users);
      } catch (e) {
        returnError(res, 500, e);
      }
    });


  router
    .route('/getAll')
    .get(async (req, res) => {
      try {
        const users = await User.findAndCountAll();
        returnData(res, users);
      } catch (e) {
        returnError(res, 500, e);
      }
    });

  router
    .route('/by-id/:id')
    .get(async (req, res) => {
      try {
        const { id } = req.params;
        const user = await User.findByPk(id);
        delete user.password;
        returnData(res, user);
      } catch (e) {
        returnError(res, 500, e);
      }
    });

  router
    .route('/:id')
    .patch(async (req, res) => {
      try {
        const { id } = req.params;
        const { body } = req;

        const user = await User.findByPk(id);
        for (const [key, value] of Object.entries(body)) {
          user[key] = value;
        }
        delete user.password;
        await user.save();
        returnData(res, user)
      } catch (e) {
        returnError(res, 500, e);
      }
    });

  router
    .route('/type/:type')
    .get(async (req, res) => {
      try {
        const { type } = req.params;
        const offset = getOffset(req);
        if (!type) {
          returnError(res, 412, 'User type needed');
          return;
        }
        const filter = {
          attributes: ['id', 'email', 'username', 'createdAt', 'name', 'lastname', 'secondLastName'],
          include: [{
            model: Company,
          }],
          limit: 10,
          offset,
          where: {
            role: type,
          },
        };
        const users = await User.findAndCountAll(filter);
        returnData(res, users);
      } catch (e) {
        returnError(res, 500, e);
      }
    });

  router
    .route('/technicals-activity')
    .get(async (req, res) => {
      try {
        const filter = {
          where: {
            role: 'technical',
          },
          attributes: {
            include: [
              [
                Sequelize.fn('COUNT', Sequelize.col('technicalTickets.id')), 'ticketCount',
              ],
            ],
          },
          include: [
            {
              model: Ticket,
              as: 'technicalTickets',
              where: {
                status: 'assigned',
              },
              attributes: [],
            },
          ],
        };
        const filterAvailable = { where: { role: 'technical' } };
        const usersBusy = await User.findAll(filter);
        console.log('usersBusy', usersBusy);
        const usersAvailable = await User.findAll(filterAvailable);
        const users = [
          ...usersBusy,
          ...usersAvailable.filter((u) => !usersBusy.find((ub) => ub.id === u.id)),
        ];
        returnData(res, users);
      } catch (e) {
        returnError(res, 500, e);
      }
    })

  return router;
};
