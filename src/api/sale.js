import express from 'express';
import { returnError, returnData, getOffset } from './utils';

export default (sequelize, Sale) => {
  const router = express.Router();

  router
  .route('/getSalesByCliendID')
  .get(async ({body}, res) => {
      try {
          const { id } = body;
          const client = await Client.findByPk(id);
          if(client != undefined)
          {
            const sale = await salefindAll({
                where:{
                    client : id,
                    status : 1 //Terminada
                }
            })
            if(sale != undefined)
            returnData(res, sale);
          }
          else
          returnData(res, "El Cliente no existe");
      } catch (e) {
          returnError(res, 500, e);
      }
  });

  router
  .route('/finalizeSale')
  .put(async ({body}, res) => {
      try {
          const { id } = body;
          const sale = await Sale.findByPk(id);
          if(sale != undefined)// 1- Terminada, 2-Cancelada , 3- en proceso
          {
                if(sale.status == 3){
                    sale.status = 1;
                    sale.save()
                    returnData(res, "La venta fue terminada con exito");
                }
                else if(sale.status == 2)
                    returnData(res, "La venta ya fue cancelada");
                else
                    returnData(res, "La venta ya fue Culminada");
          }
          else
          returnData(res, "La Venta no existe");
      } catch (e) {
          returnError(res, 500, e);
      }
  });

  router
  .route('/addSale')
  .post(async ({body}, res) => {
      try {
          const { clientID } = body;
          console.log(body);
          const sale = await Sale.build({
              client: clientID,
              import: 0,
              status: 3
          }).save();
          returnData(res, sale);
      } catch (e) {
          console.log(e);
          returnError(res, 500, e);
      }
  });

  return router;
}