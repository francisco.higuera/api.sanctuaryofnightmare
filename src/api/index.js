import { version } from '../../package.json';
import { Router } from 'express';
import userMethods from './user';
import authMethods from './auth';
import clientMethods from './client'
import productMethods from './product';
import saleMethods from './sale';
import productSaleMethods from './ProductSale'
import models from '../models';



export default ({ db }) => {
	let api = Router();
	const {
		userModel,
		productModel,
		clientModel,
		saleModel,
		productSaleModel
	} = models(db);

	// const {
	// 	productModel,
	// } = models(db);

	// TO SYNC MODELS WITH DATABASE
	// Con este comando haces la migración a la base de datos
	// Pero te elimina toda la información que tengas cuando está en force: true
	// PANCHITO
	db.sync({ force: false });

	// userModel.sync({ force: true });
	// createAdminUser(userModel);
	// createClientUser(userModel);
	// createTechnicalUser(userModel);

	// MIDDLEWARES
	// api.all('/users*', adminPermission)

	// METHODS
	api.use('/users', userMethods(db, userModel));
	api.use('/auth', authMethods(db, userModel));
	api.use('/products',productMethods(db,productModel));
	api.use('/clients',clientMethods(db,clientModel));
	api.use('/sales',saleMethods(db,saleModel));
	api.use('/productSales',productSaleMethods(db,productSaleModel));


	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
};
